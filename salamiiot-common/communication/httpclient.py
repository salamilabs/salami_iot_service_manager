import logging
import urllib.request as urllib2

from model.util import SalamiModelUtil
from utils import get_current_milliseconds_time


class HttpClient:
    def __init__(self, url):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._url = url

    def get(self):
        if "?" in self._url:
            time_param = "&t=" + str(get_current_milliseconds_time())
        else:
            time_param = "?t=" + str(get_current_milliseconds_time())

        req = urllib2.Request(self._url + time_param)
        opener = urllib2.build_opener()
        try:
            f = opener.open(req)
            return ResponseAdapter(f)
        except urllib2.HTTPError as e:
            self._logger.exception("Get request error: ")
            return ResponseAdapter(e)


class ResponseAdapter:
    def __init__(self, response):
        self.__response = response

    def object(self, clazz):
        return SalamiModelUtil.json_to_object(self.__response.readall().decode('utf-8'), clazz)

    def status_only(self):
        return self.__response.status
