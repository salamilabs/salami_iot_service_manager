import logging
import threading

import websocket


class SalamiSubscriber(threading.Thread):
    def __init__(self, url, callback):
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self._logger = logging.getLogger(self.__class__.__name__)

        self._url = url
        self._callback = callback

        self._subscribers = []

        self.__websocket_client = self._subscribe(url, callback)

        self.is_connected = False

    def _subscribe(self, url, callback):
        websocket_client = None
        try:
            websocket_client = websocket.WebSocketApp(url=url, on_message=self.on_message, on_error=self.on_error,
                                                      on_close=self.on_close)
            websocket_client.on_open = self.on_open
        except Exception:
            self._logger.exception("Error while subscribing to %s: ", url)
            exit(1)

        self._subscribers.append(callback)

        return websocket_client

    def publish(self, message):
        try:
            if self.is_connected:
                self.__websocket_client.send(message)
            else:
                self.reconnect()
                self.__websocket_client.send(message)
        except Exception:
            self._logger.exception("Error while sending to websocket")
            exit(-1)

    def on_open(self, ws):
        self.is_connected = True
        self._logger.info("Joined: %s", self._url)

    def on_message(self, ws, message):
        for callback in self._subscribers:
            callback(message)

    def on_error(self, ws, error):
        self._logger.error("Websocket error occurred: %s", error)

    def on_close(self, ws):
        self._logger.info("Websocket connection closed")

    def disconnect(self):
        self.__websocket_client.close()

    def reconnect(self):
        self._subscribe(self._url, self._callback)

    def run(self):
        self.__websocket_client.run_forever()
