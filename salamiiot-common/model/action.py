import SalamiConstants
from model.absmodel import ABSModel
from model.adapter import ModelAdapter
from model.util import SalamiModelUtil

GPIO_HOLD_FIELD = "HOLD"
GPIO_WAIT_FIELD = "WAIT"
GPIO_REPEAT_FIELD = "REPEAT"


class SalamiAction(ABSModel):
    def __init__(self, dictionary=None):
        self.name = ""
        self.type = 0
        self.ingredient_index = 0
        self.dataload = []

        self._update_dict(dictionary)

    def __str__(self):
        result = "{name: " + self.name + ","
        result = result + "{type: " + str(self.type) + ","
        result = result + "{ingredient_index: " + str(self.ingredient_index) + ","
        result = result + "{dataload: " + str(self.dataload) + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self.__dict__.update(dictionary)

    def is_message_action(self):
        return self.type == SalamiConstants.MESSAGE_ACTION_TYPE

    def is_gpio_action(self):
        return self.type == SalamiConstants.GPIO_ACTION_TYPE

    def get_dataload_field(self, field):
        for dataload_item in self.dataload:
            if dataload_item["name"] == field:
                return dataload_item["value"]
        return None

    def get_repeat(self):
        if self.type == SalamiConstants.GPIO_ACTION_TYPE:
            return self.get_dataload_field(GPIO_REPEAT_FIELD)
        else:
            return -1

    def get_hold(self):
        if self.type == SalamiConstants.GPIO_ACTION_TYPE:
            return self.get_dataload_field(GPIO_HOLD_FIELD)
        else:
            return -1

    def get_wait(self):
        if self.type == SalamiConstants.GPIO_ACTION_TYPE:
            return self.get_dataload_field(GPIO_WAIT_FIELD)
        else:
            return -1


class SalamiActionAdapter(ModelAdapter):
    def __init__(self, action):
        self.__action = action

    def object(self):
        return self.__action

    def json(self):
        return SalamiModelUtil.object_to_json(self.__action)


class SalamiActionBuilder:
    def __init__(self):
        self.__action = SalamiAction()

    def with_name(self, name):
        self.__action.name = name
        return self

    def of_type(self, type):
        self.__action.type = type
        return self

    def with_ingredient_index(self, ingredient_index):
        self.__action.ingredient_index = ingredient_index
        return self

    def with_dataload(self, dataload):
        self.__action.dataload = dataload
        return self

    def adapt_to(self):
        return SalamiActionAdapter(self.__action)
