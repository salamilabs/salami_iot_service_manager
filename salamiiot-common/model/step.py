from model.absmodel import ABSModel
from model.action import SalamiAction
from model.adapter import ModelAdapter
from model.condition import SalamiCondition
from model.util import SalamiModelUtil


class SalamiStep(ABSModel):
    def __init__(self, dictionary=None):
        self.crontab = ""
        self.sleep = -1
        self.conditions = []
        self.actions = []

        self._update_dict(dictionary)

    def __str__(self):
        result = "{ crontab: " + self.crontab + ","
        result = result + "sleep: " + str(self.sleep) + ","
        result = result + "conditions: " + str(self.conditions) + ","
        result = result + "actions: " + str(self.actions) + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self.__dict__.update(dictionary)

            tmp_conditions = []
            for dict_condition in self.conditions:
                condition = SalamiCondition(dict_condition)
                tmp_conditions.append(condition)
            self.conditions = tmp_conditions

            tmp_actions = []
            for dict_action in self.actions:
                action = SalamiAction(dict_action)
                tmp_actions.append(action)
            self.actions = tmp_actions


class SalamiStepAdapter(ModelAdapter):
    def __init__(self, step):
        self.__step = step

    def object(self):
        return self.__step

    def json(self):
        return SalamiModelUtil.object_to_json(self.__step)


class SalamiStepBuilder:
    def __init__(self):
        self.__step = SalamiStep()

    def with_crontab(self, crontab):
        self.__step.crontab = crontab
        return self

    def with_sleep(self, sleep):
        self.__step.sleep = sleep
        return self

    def with_condition(self, condition):
        self.__step.conditions.append(condition)
        return self

    def with_action(self, action):
        self.__step.actions.append(action)
        return self

    def adapt_to(self):
        return SalamiStepAdapter(self.__step)
