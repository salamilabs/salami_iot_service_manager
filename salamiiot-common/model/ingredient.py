from model.absmodel import ABSModel
from model.adapter import ModelAdapter
from model.util import SalamiModelUtil


class SalamiIngredient(ABSModel):
    def __init__(self, dictionary=None):
        self.index = 0
        self.name = ""
        self.code = 0
        self.description = ""
        self.category = 0
        self.behavior = 0
        self.type = 0
        self.input_pattern = ""
        self.address = 0
        self.slice = ""
        self.used = False
        self.pollable = False

        self._update_dict(dictionary)

    def __str__(self):
        result = "{index:" + str(self.index) + ","
        result = result + "name:" + self.name + ","
        result = result + "code:" + str(self.code) + ","
        result = result + "description:" + self.description + ","
        result = result + "category:" + str(self.category) + ","
        result = result + "behavior:" + str(self.behavior) + ","
        result = result + "type:" + str(self.type) + ","
        result = result + "input_pattern:" + self.input_pattern + ","
        result = result + "address:" + str(self.address) + ","
        result = result + "slice:" + self.slice + ","
        result = result + "pollable:" + str(self.pollable) + ","
        result = result + "used:" + str(self.used) + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self.__dict__.update(dictionary)
            self.category = dictionary["ingredient_class"]


class SalamiIngredientAdapter(ModelAdapter):
    def __init__(self, ingredient):
        self.__ingredient = ingredient

    def object(self):
        return self.__ingredient

    def json(self):
        return SalamiModelUtil.object_to_json(self.__ingredient)


class SalamiIngredientBuilder:
    def __init__(self):
        self.__ingredient = SalamiIngredient()

    def with_index(self, index):
        self.__ingredient.index = index
        return self

    def with_name(self, name):
        self.__ingredient.name = name
        return self

    def with_code(self, code):
        self.__ingredient.code = code
        return self

    def with_description(self, description):
        self.__ingredient.description = description
        return self

    def with_category(self, category):
        self.__ingredient.category = category
        return self

    def with_behavior(self, behavior):
        self.__ingredient.behavior = behavior
        return self

    def with_type(self, ingredient_type):
        self.__ingredient.type = ingredient_type
        return self

    def with_input_pattern(self, input_pattern):
        self.__ingredient.input_pattern = input_pattern
        return self

    def with_address(self, address):
        self.__ingredient.address = address
        return self

    def with_slice(self, salami_slice):
        self.__ingredient.slice = salami_slice
        return self

    def is_used(self, value):
        self.__ingredient.used = value
        return self

    def is_pollable(self, value):
        self.__ingredient.pollable = value
        return self

    def adapt_to(self):
        return SalamiIngredientAdapter(self.__ingredient)
