from model.absmodel import ABSModel
from model.ingredient import SalamiIngredient
from model.step import SalamiStep
from model.util import SalamiModelUtil


class SalamiRecipe(ABSModel):
    def __init__(self, dictionary=None):
        self.name = ""
        self.timeframe = ""
        self.ingredients = []
        self.steps = []
        self.loaded = False
        self.step_index = 0

        self._update_dict(dictionary)

    def __str__(self):
        result = "{name: " + self.name + ","
        result = result + "timeframe: " + self.timeframe + ","
        result = result + "ingredients: " + str(self.ingredients) + ","
        result = result + "steps: " + str(self.steps) + ","
        result = result + "loaded: " + str(self.loaded) + ","
        result = result + "step_index: " + str(self.step_index) + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            tmp_steps = []
            for dict_step in dictionary['steps']:
                step = SalamiStep(dictionary=dict_step)
                tmp_steps.append(step)

            tmp_ingredients = []
            for dict_ingredient in dictionary['ingredients']:
                ingredient = SalamiIngredient(dictionary=dict_ingredient)
                tmp_ingredients.append(ingredient)

            self.__dict__.update(dictionary)
            self.ingredients = tmp_ingredients
            self.steps = tmp_steps


class SalamiRecipeAdapter:
    def __init__(self, recipe):
        self.__recipe = recipe

    def object(self):
        return self.__recipe

    def json(self):
        return SalamiModelUtil.object_to_json(self.__recipe)


class SalamiRecipeBuilder:
    def __init__(self):
        self.__recipe = SalamiRecipe()

    def with_name(self, name):
        self.__recipe.name = name
        return self

    def with_timeframe(self, timeframe):
        self.__recipe.timeframe = timeframe
        return self

    def with_ingredient(self, ingredient):
        self.__recipe.ingredients.append(ingredient)
        return self

    def with_step(self, step):
        self.__recipe.steps.append(step)
        return self

    def is_loaded(self, value):
        self.__recipe.loaded = value
        return self

    def with_step_index(self, step_index):
        self.__recipe.step_index = step_index
        return self

    def adapt_to(self):
        return SalamiRecipeAdapter(self.__recipe)
