import SalamiConstants
from model.adapter import ModelAdapter
from model.util import SalamiModelUtil


class RootLoggerData:
    def __init__(self):
        self.id = "root"
        self.enabled = True
        self.is_root = True
        self.level = SalamiConstants.DEFAULT_LOG_LEVEL
        self.handlers = []


class LoggerData(RootLoggerData):
    def __init__(self):
        super().__init__()
        self.is_root = False
        self.triggers = []


class HandlerData:
    def __init__(self):
        self.type = "Generic"
        self.enabled = False
        self.level = SalamiConstants.DEFAULT_LOG_LEVEL
        self.format = SalamiConstants.LOG_FORMAT


class FileHandlerData(HandlerData):
    def __init__(self):
        super().__init__()
        self.type = "FileHandler"
        self.file_path = ""


class WsHandlerData(HandlerData):
    def __init__(self):
        super().__init__()
        self.type = "WsHandler"
        self.socket_url = ""


class Trigger:
    def __init__(self, name):
        self.name = name


class LoggingConfiguration:
    def __init__(self):
        self.root_logger = RootLoggerData()
        self.masterchef_logger = LoggerData()
        self.assistantchef_logger = LoggerData()


class LoggerDataAdapter(ModelAdapter):
    def __init__(self, logger_data):
        self._logger_data = logger_data

    def object(self):
        return self._logger_data

    def json(self):
        return SalamiModelUtil.object_to_json(self._logger_data)


class LoggerDataBuilder:
    def __init__(self):
        self._logger_data = LoggerData()

    def with_id(self, logger_id):
        self._logger_data.id = logger_id
        return self

    def enabled(self, value):
        self._logger_data.enabled = value
        return self

    def root(self, value):
        self._logger_data.is_root = value
        return self

    def with_level(self, level):
        self._logger_data.level = level
        return self

    def with_handler(self, handler):
        self._logger_data.handlers.append(handler)
        return self

    def with_trigger(self, trigger):
        self._logger_data.triggers.append(trigger)
        return self

    def adapt_to(self):
        return LoggerDataAdapter(self._logger_data)


class LoggingConfigurationAdapter(ModelAdapter):
    def __init__(self, logger_configuration):
        self._logger_configuration = logger_configuration

    def object(self):
        return self._logger_configuration

    def json(self):
        return SalamiModelUtil.object_to_json(self._logger_configuration)


class LoggingConfigurationBuilder:
    def __init__(self):
        self._logger_configuration = LoggingConfiguration()

    def with_root_logger(self, root_logger):
        self._logger_configuration.root_logger = root_logger
        return self

    def with_masterchef_logger(self, masterchef_logger):
        self._logger_configuration.masterchef_logger = masterchef_logger
        return self

    def with_assistantchef_logger(self, assistantchef_logger):
        self._logger_configuration.assistantchef_logger = assistantchef_logger
        return self

    def adapt_to(self):
        return LoggingConfigurationAdapter(self._logger_configuration)
