from model.absmodel import ABSModel
from model.adapter import ModelAdapter
from model.util import SalamiModelUtil

DEFAULT_ID = "MASTER_CHEF"


class SalamiMasterChefInfo(ABSModel):
    def __init__(self, dictionary=None):
        self.id = DEFAULT_ID
        self.ip = ""
        self.port = ""
        self.status = ""
        self.start_time = None

        self._update_dict(dictionary)

    def __str__(self):
        return "{id:" + self.id + "," + "ip:" + self.ip \
               + "," + "port:" + str(self.port) \
               + "," + "status:" + self.status + "," \
               + "start_time:" + str(self.start_time) + "}"

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self.__dict__.update(dictionary)


class SalamiMasterChefInfoAdapter(ModelAdapter):
    def __init__(self, master_chef_info):
        self.__master_chef_info = master_chef_info

    def object(self):
        return self.__master_chef_info

    def json(self):
        return SalamiModelUtil.object_to_json(self.__master_chef_info)


class SalamiMasterChefInfoBuilder:
    def __init__(self):
        self.__master_chef_info = SalamiMasterChefInfo()

    def with_id(self, master_chef_id):
        self.__master_chef_info.id = master_chef_id
        return self

    def with_ip(self, ip):
        self.__master_chef_info.ip = ip
        return self

    def with_port(self, port):
        self.__master_chef_info.port = port
        return self

    def with_status(self, status):
        self.__master_chef_info.status = status
        return self

    def with_start_time(self, start_time):
        self.__master_chef_info.start_time = start_time
        return self

    def adapt_to(self):
        return SalamiMasterChefInfoAdapter(self.__master_chef_info)
