from model.absmodel import ABSModel
from model.adapter import ModelAdapter
from model.util import SalamiModelUtil


class SalamiCondition(ABSModel):
    def __init__(self, dictionary=None):
        self.ingredient_index = 0
        self.check_value = 0
        self.value1 = 0
        self.value2 = ""
        self.value3 = ""
        self.check_interval = 0
        self.min = ""
        self.max = ""
        self.inertial = 0
        self.unchained = 0

        self._update_dict(dictionary)

    def __str__(self):
        result = "{ ingredient_index: " + str(self.ingredient_index) + ","
        result = result + "check_value: " + str(self.check_value) + ","
        result = result + "value1: " + str(self.value1) + ","
        result = result + "value2: " + str(self.value2) + ","
        result = result + "value3: " + str(self.value3) + ","
        result = result + "check_interval: " + str(self.check_interval) + ","
        result = result + "min: " + str(self.min) + ","
        result = result + "max: " + str(self.max) + ","
        result = result + "inertial: " + str(self.inertial) + ","
        result = result + "unchained: " + str(self.unchained) + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self.__dict__.update(dictionary)


class SalamiConditionAdapter(ModelAdapter):
    def __init__(self, action):
        self.__action = action

    def object(self):
        return self.__action

    def json(self):
        return SalamiModelUtil.object_to_json(self.__action)


class SalamiConditionBuilder:
    def __init__(self):
        self.__condition = SalamiCondition()

    def with_ingredient_index(self, ingredient_index):
        self.__condition.ingredient_index = ingredient_index
        return self

    def with_check_value(self, check_value):
        self.__condition.check_value = check_value
        return self

    def with_value1(self, value1):
        self.__condition.value1 = value1
        return self

    def with_value2(self, value2):
        self.__condition.value2 = value2
        return self

    def with_value3(self, value3):
        self.__condition.value3 = value3
        return self

    def with_check_interval(self, check_interval):
        self.__condition = check_interval
        return self

    def with_min(self, min_value):
        self.__condition.min = min_value
        return self

    def with_max(self, max_value):
        self.__condition.max = max_value
        return self

    def with_inertial(self, inertial):
        self.__condition.inertial = inertial
        return self

    def with_unchained(self, unchained):
        self.__condition.unchained = unchained
        return self

    def adapt_to(self):
        return SalamiConditionAdapter(self.__condition)
