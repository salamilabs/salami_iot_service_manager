from model.absmodel import ABSModel
from model.adapter import ModelAdapter
from model.util import NOT_PRESENT, SalamiModelUtil

from utils import get_current_milliseconds_time


class SalamiMessage(ABSModel):
    def __init__(self, dictionary=None):
        self.source = NOT_PRESENT
        self.destination = NOT_PRESENT
        self.ingredient = NOT_PRESENT
        self.command = NOT_PRESENT
        self.command_data = NOT_PRESENT
        self.timestamp = get_current_milliseconds_time()
        self.expiration = get_current_milliseconds_time() + 10000

        self._update_dict(dictionary)

    def __str__(self):
        result = "{source:" + str(self.source) + ","
        result = result + "destination:" + str(self.destination) + ","
        result = result + "ingredient:" + str(self.ingredient) + ","
        result = result + "command:" + str(self.command) + ","
        result = result + "command_data:" + str(self.command_data) + ","
        result = result + "expiration:" + str(self.expiration) + ","
        result = result + "timestamp:" + str(self.timestamp) + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self.__dict__.update(dictionary)


class SalamiMessageAdapter(ModelAdapter):
    def __init__(self, message):
        self.__message = message

    def object(self):
        return self.__message

    def json(self):
        return SalamiModelUtil.object_to_json(self.__message)


class SalamiMessageBuilder:
    def __init__(self):
        self.__message = SalamiMessage()

    def with_source(self, source):
        self.__message.source = source
        return self

    def with_destination(self, destination):
        self.__message.destination = destination
        return self

    def with_ingredient(self, ingredient):
        self.__message.ingredient = ingredient
        return self

    def with_command(self, command):
        self.__message.command = command
        return self

    def with_command_data(self, command_data):
        self.__message.command_data = command_data
        return self

    def with_timestamp(self, timestamp):
        self.__message.timestamp = timestamp
        return self

    def with_expiration(self, expiration):
        self.__message.expiration = expiration
        return self

    def adapt_to(self):
        return SalamiMessageAdapter(self.__message)
