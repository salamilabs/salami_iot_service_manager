from model.absmodel import ABSModel
from model.adapter import ModelAdapter
from model.util import SalamiModelUtil

from utils import MessageCallbackPlaceholder


class SalamiInitData(ABSModel):
    def __init__(self):
        self.toi_url = ""
        self.slice_slot = ""
        self.socket_url = ""
        self.message_callback = MessageCallbackPlaceholder

    def __str__(self):
        result = "{ toi_url: " + self.toi_url + ","
        result = result + "slice_id: " + self.slice_slot + ","
        result = result + str(self.message_callback) + "}"
        return result


class SalamiInitDataAdapter(ModelAdapter):
    def __init__(self, init_data):
        self._init_data = init_data

    def object(self):
        return self._init_data

    def json(self):
        return SalamiModelUtil.object_to_json(self._init_data)


class SalamiInitDataBuilder:
    def __init__(self):
        self._init_data = SalamiInitData()

    def with_toi_url(self, toi_url):
        self._init_data.toi_url = toi_url
        return self

    def with_socket_url(self, socket_url):
        self._init_data.socket_url = socket_url
        return self

    def with_slice_slot(self, slice_slot):
        self._init_data.slice_slot = slice_slot
        return self

    def with_message_callback(self, callback):
        self._init_data.message_callback = callback
        return self

    def adapt_to(self):
        return SalamiInitDataAdapter(self._init_data)
