from model.absmodel import ABSModel
from model.adapter import ModelAdapter
from model.ingredient import SalamiIngredient
from model.util import SalamiModelUtil


class SalamiTaskData(ABSModel):
    def __init__(self, dictionary=None):
        self.task_id = 0
        self.masterchef_id = ""
        self.toi_url = ""
        self.socket_url = ""
        self.timeframe = 0
        self.step = object()
        self.ingredients = []

        self._update_dict(dictionary)

    def __str__(self):
        result = "{task_id: " + str(self.task_id) + ","
        result = result + "masterchef_id: " + self.masterchef_id + ","
        result = result + "toi_url: " + self.toi_url + ","
        result = result + "socket_url: " + self.socket_url + ","
        result = result + "timeframe: " + str(self.timeframe) + ","
        result = result + "step: " + str(self.step) + ","
        result = result + "ingredients: " + str(self.ingredients) + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self.__dict__.update(dictionary)

            tmp_ingredients = []
            for dict_ingredient in self.ingredients:
                ingredient = SalamiIngredient(dict_ingredient)
                tmp_ingredients.append(ingredient)
            self.ingredients = tmp_ingredients


class SalamiTaskDataAdapter(ModelAdapter):
    def __init__(self, task_data):
        self.__task_data = task_data

    def object(self):
        return self.__task_data

    def json(self):
        return SalamiModelUtil.json_to_object(self.__task_data, SalamiTaskData)


class SalamiTaskDataBuilder:
    def __init__(self):
        self.__task_data = SalamiTaskData()

    def with_task_id(self, task_id):
        self.__task_data.task_id = task_id
        return self

    def with_masterchef_id(self, masterchef_id):
        self.__task_data.masterchef_id = masterchef_id
        return self

    def with_toi_url(self, toi_url):
        self.__task_data.toi_url = toi_url
        return self

    def with_socket_url(self, socket_url):
        self.__task_data.socket_url = socket_url
        return self

    def with_timeframe(self, timeframe):
        self.__task_data.timeframe = timeframe
        return self

    def with_step(self, step):
        self.__task_data.step = step
        return self

    def with_ingredient(self, ingredient):
        self.__task_data.ingredients.append(ingredient)
        return self

    def with_ingredients(self, ingredients):
        self.__task_data.ingredients = ingredients
        return self

    def adapt_to(self):
        return SalamiTaskDataAdapter(self.__task_data)


class SalamiTransactionInfo(ABSModel):
    def __init__(self, dictionary=None):
        self.start_time = 0
        self.end_time = 0
        self.active = False

        self._update_dict(dictionary)

    def __str__(self):
        result = "{start_time: " + str(self.start_time) + ","
        result = result + "end_time: " + str(self.end_time) + ","
        result = result + "active: " + str(self.active) + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self.__dict__.update(dictionary)


class SalamiTransactionInfoAdapter(ModelAdapter):
    def __init__(self, transaction_info):
        self.__transaction_info = transaction_info

    def object(self):
        return self.__transaction_info

    def json(self):
        return SalamiModelUtil.object_to_json(self.__transaction_info)


class SalamiTransactionInfoBuilder:
    def __init__(self):
        self.__transaction_info = SalamiTransactionInfo()

    def with_start_time(self, start_time):
        self.__transaction_info.start_time = start_time
        return self

    def with_end_time(self, end_time):
        self.__transaction_info.end_time = end_time
        return self

    def is_active(self, value):
        self.__transaction_info.active = value

    def adapt_to(self):
        return SalamiTransactionInfoAdapter(self.__transaction_info)


class SalamiTaskReport(ABSModel):
    def __init__(self, dictionary=None):
        self.task_id = 0
        self.task_counter = 0
        self.start_time = 0
        self.end_time = 0
        self.exit_status = ""
        self.execution_data = ""

        self._update_dict(dictionary)

    def __str__(self):
        result = "{task_id: " + str(self.task_id) + ","
        result = result + "task_counter: " + str(self.task_counter) + ","
        result = result + "start_time: " + str(self.start_time) + ","
        result = result + "end_time: " + str(self.end_time) + ","
        result = result + "exit_status: " + self.exit_status + ","
        result = result + "execution_data: " + self.execution_data + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self.__dict__.update(dictionary)


class SalamiTaskReportAdapter(ModelAdapter):
    def __init__(self, task_report):
        self.__task_report = task_report

    def object(self):
        return self.__task_report

    def json(self):
        return SalamiModelUtil.object_to_json(self.__task_report)


class SalamiTaskReportBuilder:
    def __init__(self):
        self.__task_report = SalamiTaskReport()

    def with_task_id(self, task_id):
        self.__task_report.task_id = task_id
        return self

    def with_task_counter(self, total_tasks):
        self.__task_report.task_counter = total_tasks
        return self

    def with_start_time(self, start_time):
        self.__task_report.start_time = start_time
        return self

    def with_end_time(self, end_time):
        self.__task_report.end_time = end_time
        return self

    def with_exit_status(self, exit_status):
        self.__task_report.exit_status = exit_status
        return self

    def with_execution_data(self, execution_data):
        self.__task_report.execution_data = execution_data
        return self

    def adapt_to(self):
        return SalamiTaskReportAdapter(self.__task_report)


class SalamiTaskCounter(ABSModel):
    def __init__(self, dictionary=None):
        self.completed_tasks = 0
        self.timed_out_tasks = 0

        self._update_dict(dictionary)

    def __str__(self):
        result = "{completed_tasks: " + str(self.completed_tasks) + ","
        result = result + "timed_out_tasks: " + str(self.timed_out_tasks) + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self.__dict__.update(dictionary)

    def add_completed_task(self):
        self.completed_tasks += 1

    def add_timed_out_task(self):
        self.timed_out_tasks += 1

    def get_total_task_counter(self):
        return self.completed_tasks + self.timed_out_tasks
