import json

import SalamiConstants

NOT_PRESENT = "NOT_PRESENT"


def j_default(o):
    return o.__dict__


class SalamiModelUtil:
    def __init__(self):
        pass

    @staticmethod
    def json_to_object(json_message, clazz):
        to_dict = json.JSONDecoder().decode(json_message)
        return clazz(dictionary=to_dict)

    @staticmethod
    def object_to_json(object_message):
        return json.dumps(object_message, default=j_default)

    @staticmethod
    def verify_message(message):
        verified = True
        if message.source == NOT_PRESENT:
            verified = False
        if message.destination == NOT_PRESENT:
            verified = False
        if message.ingredient == NOT_PRESENT:
            verified = False
        if message.command == NOT_PRESENT:
            verified = False
        if message.command_data == NOT_PRESENT:
            verified = False
        return verified

    @staticmethod
    def find_one_by_name(source_list, name):
        result = [element for element in source_list if element.ingredient_name == name]
        return result[0] if len(result) == 1 else None

    @staticmethod
    def find_one_by_index(source_list, index):
        result = [element for element in source_list if element.ingredient_index == index]
        return result[0] if len(result) == 1 else None

    @staticmethod
    def get_action_command(ingredient_type):
        if int(ingredient_type) == SalamiConstants.GPIO_ACTION_TYPE:
            return SalamiConstants.CMD_DO_GPIO_ACTION
        if int(ingredient_type) == SalamiConstants.MESSAGE_ACTION_TYPE:
            return SalamiConstants.CMD_DO_MESSAGE_ACTION
