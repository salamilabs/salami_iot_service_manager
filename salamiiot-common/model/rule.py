from model.absmodel import ABSModel
from model.adapter import ModelAdapter
from model.condition import SalamiCondition
from model.util import SalamiModelUtil


class SalamiRuleData(ABSModel):
    def __init__(self, dictionary=None):
        self.ingredient_index = 0
        self.condition = SalamiCondition()

        self._update_dict(dictionary)

    def __str__(self):
        result = "{ingredient_index: " + str(self.ingredient_index) + ","
        result = result + "condition: " + str(self.condition) + "}"
        return result

    def _update_dict(self, dictionary):
        if dictionary is not None:
            self._update_dict(dictionary)
            dict_condition = self.condition
            tmp_condition = SalamiCondition(dict_condition)
            self.condition = tmp_condition


class SalamiRuleDataAdapter(ModelAdapter):
    def __init__(self, rule_data):
        self.__rule_data = rule_data

    def object(self):
        return self.__rule_data

    def json(self):
        return SalamiModelUtil.object_to_json(self.__rule_data)


class SalamiRuleDataBuilder:
    def __init__(self):
        self.__rule_data = SalamiRuleData()

    def with_ingredient_index(self, ingredient_index):
        self.__rule_data.ingredient_index = ingredient_index
        return self

    def with_condition(self, condition):
        self.__rule_data.condition = condition
        return self

    def adapt_to(self):
        return SalamiRuleDataAdapter(self.__rule_data)
