import os

import requests
from flask import Flask, request
from flask.ext.cors import CORS
from flask_jsonrpc import JSONRPC
from flask_restful import Resource, Api

from security import SecurityManager
from services import ServiceController, ServiceData
from services_configuration import ServiceConfigurationManager

DATA_FOLDER_PATH = os.path.abspath("/opt/salamiiot/data/")
KEY_FILE_PATH = os.path.join(DATA_FOLDER_PATH, "key.salami")
PASSPORT_FILE_PATH = os.path.join(DATA_FOLDER_PATH, "passport.salami")

app = Flask(__name__)
jsonrpc = JSONRPC(app, '/methods')
api = Api(app)
CORS(app)

service_controller = ServiceController()
services_configuration_manager = ServiceConfigurationManager()


class Services(Resource):
    def get(self):
        return services_configuration_manager.get_services_configuration()


class ServicesView(Resource):
    def get(self):
        services = services_configuration_manager.get_services_info()
        for service in services:
            status = service_controller.get_service_status(service)
            services[service]['status'] = status
        return services


class Service(Resource):
    def get(self, service_id):
        return services_configuration_manager.get_service_configuration(service_id)

    def put(self, service_id):
        configuration = {'address': request.form['address'], 'port': request.form['port']}
        services_configuration_manager.update_service_configuration(service_id, configuration)


class ServicesStatus(Resource):
    def get(self):
        services = services_configuration_manager.get_services_configuration()
        statuses = []
        for service in services:
            status = service_controller.get_service_status(service)
            statuses.append(status)
        return statuses


class ServiceStatus(Resource):
    def get(self, service_id):
        return service_controller.get_service_status(service_id)


api.add_resource(Services, '/services/configuration/')
api.add_resource(ServicesView, '/services/')
api.add_resource(Service, '/services/configuration/<string:service_id>')
api.add_resource(ServicesStatus, '/services/status/')
api.add_resource(ServiceStatus, '/services/status/<string:service_id>')


@jsonrpc.method('Manager.startServices')
def start_services():
    services = services_configuration_manager.get_services_configuration()
    for service_name in services:
        service = services[service_name]
        data = ServiceData(service_name, service['start'], [], service['sudo'])
        service_controller.start_service(data)
    return u'Start services command executed'


@jsonrpc.method('Manager.stopServices')
def stop_services():
    services = services_configuration_manager.get_services_configuration()
    for service in services:
        data = ServiceData(service.name, service.stop, [], service.sudo)
        service_controller.stop_service(data)
    return u'Stop services command executed'


@jsonrpc.method('Manager.startService')
def start_service(service_id):
    service = services_configuration_manager.get_service_configuration(service_id)
    args = {}
    if 'configuration' in service:
        args = {"address": "--bind " + service['configuration']['address'],
                "port": "--port " + service['configuration']['port']}
    data = ServiceData(service['name'], service['start'], args, service['sudo'])
    service_controller.start_service(data)
    return u'Start {0} service command executed'.format(service_id)


@jsonrpc.method('Manager.stopService')
def stop_service(service_id):
    service = services_configuration_manager.get_service_configuration(service_id)
    data = ServiceData(service['name'], service['stop'], [], service['sudo'])
    service_controller.stop_service(data)
    return u'Stop {0} service command executed'.format(service_id)


@jsonrpc.method('Manager.join')
def join(params):
    security_manager = SecurityManager()
    security_manager.save_key(params['key'])
    security_manager.generate_passport(params['content'])
    data = {"suffix": params['key']}
    response = requests.post('http://127.0.0.1:8000/upload/',
                             files={'passport.salami': open('/opt/salamiiot/data/passport.salami', 'rb')}, data=data)
    remote_path = response.content.decode("UTF-8")
    return {"status": "OK", "path": remote_path}


@jsonrpc.method('Manager.isFirstAccess')
def is_first_access():
    if os.path.isfile(KEY_FILE_PATH) and os.path.isfile(PASSPORT_FILE_PATH):
        return u'false'
    else:
        return u'true'


if __name__ == '__main__':
    app.run(debug=True)
