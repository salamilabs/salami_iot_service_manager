import yaml
import os


class ServiceConfigurationManager:
    def __init__(self):
        cwd = os.path.dirname(os.path.abspath(__file__))
        with open(cwd + '/services.yml', 'r') as f:
            self._configuration = yaml.load(f)

    def get_services_configuration(self):
        return self._configuration['services']

    def get_services_info(self):
        return ServiceConfigurationManager._remove_commands(self._configuration['services'])

    def get_service_configuration(self, service_id):
        return self._configuration['services'][service_id]

    def update_service_configuration(self, service_id, configuration):
        self._configuration['services'][service_id]['configuration'] = configuration
        self._save_configuration()

    def _save_configuration(self):
        out_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "services.yml")
        with open(out_file_path, 'w') as outfile:
            outfile.write(yaml.dump(self._configuration, default_flow_style=True))

    @staticmethod
    def _remove_commands(configuration):
        cp = configuration.copy()
        for service in cp:
            del cp[service]['start']
            del cp[service]['stop']
        return cp
