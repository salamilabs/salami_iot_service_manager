import os
import logging


class ServiceController:
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)

    def start_service(self, data):
        self._logger.info("Trying to start service %s, with command path %s", data.name, data.path)
        return self._launch_command(data.path, data.args, data.sudo)

    def stop_service(self, data):
        self._logger.info("Trying to stop service %s, with command path %s", data.name, data.path)
        return self._launch_command(data.path, data.sudo, {})

    def _launch_command(self, command_path, args, sudo):
        str_args = " ".join("{!s}".format(val) for (key, val) in args.items())
        command_path = command_path + " " + str_args
        if sudo:
            self._logger.warn("Executing command with superuser privileges")
            command_str = "sudo " + command_path
            self._logger.debug("Sudo command is: %s", command_str)
        else:
            command_str = command_path
        return os.system(command_str)

    def get_service_status(self, service_id):
        result = self._launch_command("pgrep " + service_id, {}, False)
        status = {"name": service_id}
        if result == 0:
            status['state'] = "RUNNING"
        elif result == 256:
            status['state'] = "STOPPED"
        else:
            status['state'] = "ERROR"
        return status


class ServiceData:
    def __init__(self, name, path, args, sudo=False):
        self.name = name
        self.sudo = sudo
        self.path = path
        self.args = args
